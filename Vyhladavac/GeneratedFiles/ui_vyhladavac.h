/********************************************************************************
** Form generated from reading UI file 'vyhladavac.ui'
**
** Created by: Qt User Interface Compiler version 5.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_VYHLADAVAC_H
#define UI_VYHLADAVAC_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_VyhladavacClass
{
public:
    QWidget *centralWidget;
    QLineEdit *lineEdit;
    QListWidget *listWidget;
    QPushButton *pushButton;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *VyhladavacClass)
    {
        if (VyhladavacClass->objectName().isEmpty())
            VyhladavacClass->setObjectName(QStringLiteral("VyhladavacClass"));
        VyhladavacClass->resize(339, 375);
        centralWidget = new QWidget(VyhladavacClass);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        lineEdit = new QLineEdit(centralWidget);
        lineEdit->setObjectName(QStringLiteral("lineEdit"));
        lineEdit->setGeometry(QRect(10, 10, 311, 20));
        listWidget = new QListWidget(centralWidget);
        listWidget->setObjectName(QStringLiteral("listWidget"));
        listWidget->setGeometry(QRect(10, 70, 311, 251));
        pushButton = new QPushButton(centralWidget);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(130, 40, 75, 23));
        VyhladavacClass->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(VyhladavacClass);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 339, 21));
        VyhladavacClass->setMenuBar(menuBar);
        mainToolBar = new QToolBar(VyhladavacClass);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        VyhladavacClass->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(VyhladavacClass);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        VyhladavacClass->setStatusBar(statusBar);

        retranslateUi(VyhladavacClass);
        QObject::connect(pushButton, SIGNAL(clicked()), VyhladavacClass, SLOT(Go()));
        QObject::connect(listWidget, SIGNAL(itemSelectionChanged()), VyhladavacClass, SLOT(Internet()));

        QMetaObject::connectSlotsByName(VyhladavacClass);
    } // setupUi

    void retranslateUi(QMainWindow *VyhladavacClass)
    {
        VyhladavacClass->setWindowTitle(QApplication::translate("VyhladavacClass", "Vyhladavac", 0));
        pushButton->setText(QApplication::translate("VyhladavacClass", "Go", 0));
    } // retranslateUi

};

namespace Ui {
    class VyhladavacClass: public Ui_VyhladavacClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_VYHLADAVAC_H
