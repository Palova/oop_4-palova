#ifndef VYHLADAVAC_H
#define VYHLADAVAC_H

#include <QtWidgets/QMainWindow>
#include "ui_vyhladavac.h"
#include <QNetworkAccessManager>
#include <QUrl>
#include <QtNetwork>
#include <QString>
#include <QListWidgetItem>
#include<qregularexpression.h>
#include <qdesktopservices.h>

class Vyhladavac : public QMainWindow
{
	Q_OBJECT

public:
	Vyhladavac(QWidget *parent = 0);
	~Vyhladavac();
	
	public slots:
	void Go();
	void citaj_stranku();
	void Internet();

private:
	Ui::VyhladavacClass ui;
	QUrl url;
	QNetworkAccessManager qnam;
	QNetworkReply *reply;

	QString pripoj;
	QStringList regVyraz2;
};

#endif // VYHLADAVAC_H
